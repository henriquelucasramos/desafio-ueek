<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
</head>


<div style="text-align: center; font-family: 'Rubik', sans-serif; padding: 40px 200px">

    <h1 style="font-style: normal;
    font-weight: 300;
    font-size: 32px;">Você se inscreveu com sucesso na nossa <b>newsletter!</b></h1>

    <img src=" {{ url('img/check.png') }}" style="max-width: 100%;
    width: 130px;
    height: 130px;
    margin: 50px 0;">

    <p style="font-weight: 300;
    font-size: 18px">Muito obrigado <b>{{ $nome }}!</b> <br>
        Agora você receberá semanalmente nossa newsletter sobre
        produtos e promoções do nosso site! Ficamos muito contente
        em tê-lo por aqui!
    </p>

    <ul style="list-style: none; margin-top: 30px">
        <li><b>Nome: </b>{{ $nome }} </li>
        <li><b>E-mail: </b>{{ $email }} </li>
        <li><b>Data: </b>{{ $data }} </li>
    </ul>


</div>
