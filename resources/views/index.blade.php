@extends('app')

@section('titulo', 'Shopmee Newsletter')

@section('conteudo')

<div class="container index mt-5">

    <img src="{{ url('img/brand.png') }}" class="brand">

    <p class="welcome-text">
        Inscreva-se em nossa newsletter e receba semanalmente <b>conteúdo sobre negócios e dicas para aumentar suas
            vendas.</b>
    </p>

    <form action="{{ route('shopmee.store') }}" method="POST" enctype="multipart/form-data">

        @include('_form')

        <button type="submit" class="btn btn-subs">me inscrever</button>

    </form>

    <a href="{{ route('shopmee.list') }}" class="btn btn-subs orange">acessar visitantes</a>

</div>

@endsection
