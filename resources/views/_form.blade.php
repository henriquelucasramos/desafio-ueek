@csrf

<div class="mb-3">
    <input value="{{ @$visitante->email }}" type="email" class="form-control input-form" id="email" name="email"
        required maxlength="150" placeholder="Comece informando seu melhor e-mail">
</div>
<div class="mb-3">
    <input value="{{ @$visitante->nome_visitante }}" type="text" class="form-control input-form" id="nome_visitante"
        name="nome_visitante" placeholder="Agora o seu nome">
</div>
