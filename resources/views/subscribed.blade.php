@extends('app')

@section('titulo', 'Inscrito com sucesso!')

@section('conteudo')

<div class="container index pt-4 mt-5">
    <img src="{{ url('img/brand.png') }}" class="brand subs">

    <img src="{{ url('img/check.png') }}" class="check">

    <p>Você se inscreveu com <b>sucesso!</b></p>

    <a href="{{ route('shopmee.index') }}" class="btn btn-subs orange">Voltar ao início</a>
</div>


@endsection
