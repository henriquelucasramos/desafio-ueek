@extends('app')

@section('titulo', 'Shopmee Newsletter')

@section('conteudo')

<div class="container index pt-4 mt-5">

    <img src="{{ url('img/brand.png') }}" class="brand subs">

    <h1 class="list-title my-5 fw-normal">Atualizar dados do visitante</h1>


    <form action="{{ route('shopmee.update', $visitante) }}" method="POST" enctype="multipart/form-data">

        @method('PUT')

        @include('_form')

        <button type="submit" class="btn btn-subs">Atualizar dados</button>

    </form>

</div>
