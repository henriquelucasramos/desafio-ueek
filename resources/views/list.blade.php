@extends('app')

@section('titulo', 'Atualizar dados')

@section('conteudo')

<div class="container text-center">

    <h1 class="my-5 fw-normal">Lista de Visitantes</h1>


    <div class="row">
        @forelse ($visitantes as $visitante)
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <div class="header-card">
                        <span class="card-title">{{ $visitante->nome_visitante }}</span>
                        <a href="{{ route('shopmee.destroy', $visitante) }}" class="delete-btn">
                            <img class="delete-icon" src="{{ url('img/delete.png') }}">
                        </a>
                    </div>
                    <p class="card-text">{{ $visitante->email }}</p>
                    <a href="{{ route('shopmee.edit', $visitante) }}" class="btn btn-subs orange">Atualizar dados</a>
                </div>
            </div>
        </div>
        @empty
        <div class="index mx-auto pt-4">
            <img src="{{ url('img/brand.png') }}" class="brand subs">
            <p class="text-center"><b>Nenhum</b> registro cadastrado</p>
            <a href="{{ route('shopmee.index') }}" class="btn btn-subs orange">Cadastrar um visitante</a>
        </div>
        @endforelse
    </div>
    <a href="{{ route('shopmee.export') }}" class="btn btn-subs export-btn">Exportar para planilha</a>
</div>

@endsection
