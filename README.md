Para rodar o sistema é necessário instalar as dependências utilizando os comandos
"npm install" e "composer install" no terminal do vscode

Criar uma database no mysql chamada "shopmee" e rodar o comando "php artisan migrate" no terminal do vscode
para criar as tabelas no banco

Utilizar o comando "php artisan serve" para abrir o servidor e abrir a página no link mostrado

