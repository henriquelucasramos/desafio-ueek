<?php

use App\Http\Controllers\visitanteController;
use App\Mail\newShopmee;
use App\Models\Visitante;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [VisitanteController::class, 'index'])->name('shopmee.index');
Route::post('/create', [VisitanteController::class, 'store'])->name('shopmee.store');
Route::get('/inscrito-com-sucesso', [VisitanteController::class, 'subscribed'])->name('shopmee.subscribed');
Route::get('/visitantes', [VisitanteController::class, 'visitorsList'])->name('shopmee.list');
Route::get('/visitantes/{id}/edit', [VisitanteController::class, 'edit'])->name('shopmee.edit');
Route::put('/visitantes/{id}', [VisitanteController::class, 'update'])->name('shopmee.update');
Route::get('/visitantes/{id}', [VisitanteController::class, 'destroy'])->name('shopmee.destroy');
Route::get('/export', [VisitanteController::class, 'export'])->name('shopmee.export');