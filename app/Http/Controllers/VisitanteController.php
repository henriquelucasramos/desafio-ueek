<?php

namespace App\Http\Controllers;

use App\Exports\VisitantesExport;
use App\Models\Visitante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class visitanteController extends Controller
{

    // Retorna a view index.blade.php

    public function index()
    {
        return view('index');
    }

    // Busca os dados no banco e retorna junto com a view list.blade.php

    public function visitorsList()
    {
        $visitantes = Visitante::get();
        
        return view('list', [
            'visitantes' => $visitantes
        ]);
    }

    // Armazena os dados do formulário no banco com excessão do '_token'
    // e envia os dados por email

    public function store(Request $request)
    {
        $dados = $request->except('_token');

        Visitante::create($dados);

        $dados_email = array(
        'nome' => $dados['nome_visitante'],
        'email' => $dados['email'],
        'data' => date("d/m/Y")
        );

        Mail::send('mail.newShopmee', $dados_email, function ($message) use ($dados_email) {
        $message->to($dados_email['email'], $dados_email['nome'])
        ->subject('Obrigado por se cadastrar!')
        ->from('henrique.lucasramos@gmail.com', 'Shopmee');
        });

        return redirect()->route('shopmee.subscribed');
    }

    // Retorna a view subscribed.blade.php

    public function subscribed()
    {
        return view('subscribed');
    }

    // Busca os dados no banco através do id
    // e retorna um formulário com os dados prontos para serem atualizados

    public function edit(int $id)
    {
        $visitante = Visitante::findOrFail($id);

        return view('edit', [
            'visitante' => $visitante
        ]);
    }

    // Busca os dados no banco através do id
    // armazena os dados requisitados, realiza atualização no banco
    // e retorna para a view list.blade.php

    public function update(int $id, Request $request)
    {
        $visitante = Visitante::findOrFail($id);

        $dados = $request->except(['_token', '_method']);

        $visitante->update($dados);

        return redirect()->route('shopmee.list');
    }

    // Busca o visitante no banco através do id
    // e realiza a exclusão

    public function destroy(int $id)
    {
        $visitante = Visitante::findOrFail($id);

        $visitante->delete();

        return redirect()->route('shopmee.list');
    }

    // Exporta a lista de visitantes para um arquivo .xlsx

    public function export()
    {
        return Excel::download(new VisitantesExport, 'lista-de-visitantes.xlsx');
    }
}